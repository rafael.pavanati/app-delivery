import angular from 'angular';

import {default as uiRouter} from '@uirouter/angularjs';
import diretivas from './diretivas';

import {mainConfig} from './main/config';
import {contasConfig} from './contas/config';
import {regrasConfig} from "./regras/config";

const modulo = angular
    .module('app', [uiRouter, diretivas]);

export default modulo
    .config(mainConfig)
    .config(contasConfig(modulo))
    .config(regrasConfig(modulo))
    .constant('BASE_URL', 'http://localhost:8088/api')
    .name;
