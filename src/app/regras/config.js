import ListController from './list.controller'
import FormController from './form.controller'
import RegrasService from './service';

export const regrasConfig = (modulo) => {
  modulo.service('RegrasService', RegrasService);
  return ['$stateProvider', config];
};

function config ($stateProvider) {
  $stateProvider
    .state('app.regras', {
      template: require('@views/default.html'),
      url: 'regras',
      redirectTo: 'app.regras.list'
    })
    .state('app.regras.list', {
      template: require('@views/regras/list.html'),
      url: '/list',
      controller: ListController,
      controllerAs: 'vm'
    })
    .state('app.regras.new', {
      template: require('@views/regras/form.html'),
      url: '/new',
      controller: FormController,
      controllerAs: 'vm'
    })
    .state('app.regras.edit', {
      template: require('@views/regras/form.html'),
      url: '/{id}',
      controller: FormController,
      controllerAs: 'vm'
    });
}