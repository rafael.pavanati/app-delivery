export default class FormController {

    constructor(RegrasService, $stateParams, $state) {
        this.service = RegrasService;
        this.$state = $state;
        if ($stateParams.id) {
            this.service.getOne($stateParams.id)
                .then(registro => {
                    this.record = registro;
                }).catch(console.error);
        }
    }

    save() {
        this.service.save(this.record)
            .then(() => {
                this.$state.go('app.regras.list')
            }).catch(console.error);
    }


}

FormController.$inject = [
    'RegrasService',
    '$stateParams',
    '$state'
];
