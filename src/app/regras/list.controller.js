export default class ListController {
    excluir(data) {
        this.service.remove(data)
            .then(() => {
                this.findAll()
            }).catch(console.error);
    }

    findAll() {
        this.service.getAll()
            .then(records => {
                this.records = records.content;
            }).catch(erro => {
            console.log(erro);
        });
    }

    constructor(RegrasService) {
        this.service = RegrasService;
        this.cols = [
            {
                label: 'Multa',
                value: 'multa',
                formatter: function (data) {
                    return `${data} %`
                }
            },
            {
                label: 'Dias',
                value: 'diasAtraso',
                formatter: function (data) {
                    return `${data | 1} dia(s) ou mais`
                }
            },
            {
                label: 'Juros dia',
                value: 'jurosDia',
                formatter: function (data) {
                    return `${data} %`
                }
            },
        ];

        this.records = [];

        this.findAll()
    }
}

ListController.$inject = ['RegrasService']