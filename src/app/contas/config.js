import ListController from './list.controller'
import FormController from './form.controller'
import ContasService from './service';

export const contasConfig = (modulo) => {
  modulo.service('ContasService', ContasService);
  return ['$stateProvider', config];
};

function config ($stateProvider) {
  $stateProvider
    .state('app.contas', {
      template: require('@views/default.html'),
      url: 'clientes',
      redirectTo: 'app.contas.list'
    })
    .state('app.contas.list', {
      template: require('@views/contas/list.html'),
      url: '/list',
      controller: ListController,
      controllerAs: 'vm'
    })
    .state('app.contas.new', {
      template: require('@views/contas/form.html'),
      url: '/new',
      controller: FormController,
      controllerAs: 'vm'
    })
    .state('app.contas.edit', {
      template: require('@views/contas/form.html'),
      url: '/{id}',
      controller: FormController,
      controllerAs: 'vm'
    });
}