export default class ListController {
    excluir(data) {
        this.service.remove(data)
            .then(() => {
                this.findAll()
            }).catch(console.error);
    }

    findAll() {
        this.service.getAll()
            .then(records => {
                this.records = records.content;
            }).catch(erro => {
            console.log(erro);
        });
    }

    constructor(ContasService) {
        this.service = ContasService;
        this.cols = [
            {
                label: 'Nome',
                value: 'nome'
            },
            {
                label: 'Data Vencimento',
                value: 'dataVencimento'
            },
            {
                label: 'Data Pagamento',
                value: 'dataPagamento'
            },
            {
                label: 'Valor',
                value: 'valor',
                formatter: function (data) {
                    return data.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'})
                }
            },
            {
                label: 'Valor corrigido',
                value: 'valorCorrigido',
                formatter: function (data) {
                    return data ? data.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'}) : '-'
                }
            },
            {
                label: 'Atraso',
                value: 'diasEmAtraso',
                formatter: function (data) {
                    if (data > 0) {
                        return `${data} dias em atraso!`
                    } else {
                        return 'conta em dia!'
                    }
                }
            }];

        this.records = [];

        this.findAll()
    }
}

ListController.$inject = ['ContasService']