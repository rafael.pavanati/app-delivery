export default class FormController {

    constructor(ContasService, $stateParams, $state) {
        this.service = ContasService;
        this.$state = $state;
        if ($stateParams.id) {
            this.service.getOne($stateParams.id)
                .then(registro => {
                    this.record = registro;
                    this.record.dataVencimento = new Date(registro.dataVencimento) ;
                    if(registro.dataPagamento){
                        this.record.dataPagamento = new Date(registro.dataPagamento) ;
                    }
                }).catch(console.error);
        }
    }

    save() {
        this.service.save(this.record)
            .then(() => {
                this.$state.go('app.contas.list')
            }).catch(console.error);
    }


}

FormController.$inject = [
    'ContasService',
    '$stateParams',
    '$state'
];
