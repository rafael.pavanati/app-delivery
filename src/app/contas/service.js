export default class ContasService {

    constructor($http, BASE_URL) {
        this.$http = $http;
        this.baseUrl = `${BASE_URL}/conta`;
    }

    getAll() {
        return this.$http.get(this.baseUrl)
            .then(response => response.data);
    }

    getOne(id) {
        return this.$http.get(`${this.baseUrl}/${id}`)
            .then(response => response.data);
    }

    save(data) {
        return this.$http.post(this.baseUrl, data)
            .then(response => response.data);
    }

    remove(id) {
        return this.$http.delete(`${this.baseUrl}/${id}`);
    }

}

ContasService.$inject = ['$http', 'BASE_URL'];