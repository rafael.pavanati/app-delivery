import MenuController from './menu.controller';

import '../../assets/bootstrap-solid.svg';

export const mainConfig = ($stateProvider, $urlRouterProvider) => {
  $urlRouterProvider.otherwise('/');
  $stateProvider
    .state('app', {
      template: require('@views/template.html'),
      redirectTo: 'app.contas',
      controller: MenuController,
      controllerAs: 'vm',
      url: '/'
    })
};

mainConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
