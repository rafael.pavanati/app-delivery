export default class MenuController {
    constructor($state) {
        this.itens = [
            {
                state: 'app.contas',
                icon: 'icon-file',
                name: 'Contas'
            },
            {
                state: 'app.regras',
                icon: 'icon-file',
                name: 'Regras'
            }
        ]

        this.$state = $state;
    }

}

MenuController.$inject = ['$state'];
